export interface OnBeforeUpdate {
    beforeUpdate(): void;
}
