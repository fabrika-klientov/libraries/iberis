export interface OnBeforeCreate {
    beforeCreate(): void;
}
