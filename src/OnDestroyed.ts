export interface OnDestroyed {
    destroyed(): void;
}
